﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using LoginPrac.Models;
using LoginPrac.Data;

namespace LoginPrac.Controllers
{
    public class LoginController : Controller
    {
        private readonly LoginContext _context;
        public const string ActiveUser = "User Error Has Occurred";

        public LoginController(LoginContext context)
        {
            _context = context;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Registering([Bind("Id, Username, Password, FirstName, LastName, Email, Mobile")] User user)
        {
            if (ModelState.IsValid)
            {
                _context.Add(user);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Home");
            }

            return RedirectToAction("Error", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(string username, string password)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    var SearchUser = await _context.Users.SingleOrDefaultAsync(check => check.Username == username && check.Password == password);
                    if (SearchUser != null)
                    {
                        HttpContext.Session.SetString(ActiveUser, username);
                        return RedirectToAction("Completed", "Login");
                    }
                }
                catch { }
            }

            return RedirectToAction("Error", "Home");
        }

        public IActionResult Completed()
        {
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }
    }
}