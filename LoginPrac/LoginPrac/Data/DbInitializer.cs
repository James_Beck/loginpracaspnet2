﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoginPrac.Data;

namespace LoginPrac.Data
{
    public class DbInitializer
    {
        public static void Initialize(LoginContext context)
        {
            context.Database.EnsureCreated();
        }
    }
}
